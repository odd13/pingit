# pinGit

## Requirements:
Can you write a small Ruby based script/app that will get HTTP response times over 5 minutes from your location to https://gitlab.com?

* Ruby version: 2.6.3

* System dependencies
  * bundler also need to be installed
  * git

## Main Script (pinGit.rb):

Main script will run the defaults listed in the service with the added host of `https://gitlab.com`. This will take 5 minutes to run (if defaults are left) and will display the results in an array. There will also be a small summary of statistics on the returned array.

The below command downloads to your home directory and is named `pinGit`, adjust this to suit your projects folder.

from the command line:
```
git clone git@gitlab.com:odd13/pingit.git ~/pinGit
```
Then go to the directory:
```
cd ~/pinGit
```
Run bundle install to install the required gems:
```
bundle install
```
Run the tests to make sure it is all working:

```
bundle exec rspec
```

Now you will have the script locally and you only need to run:
```
bundle exec ruby pinGit.rb
```

This should then return an Array of ping times every 5 seconds over a 5 minute period. If you wish to adjust these times, do so by editing the initialisation of PingResponseTimesService.new(). Find below what the service accepts.

## Service:
- Name: PingResponseTimes
- Accepts: host:, time_to_ping_in_seconds:, ping_intervals_in_seconds:
- Returns: Array of ping reponse times


## Tests
Running the tests requires rspec, in the terminal:
1. Run `bundle install`
1. Run `bundle exec rspec`

# Though process:
I though about this and found the best way to tackle this was to create a service that accepts settings for a host, interval times and length of time to ping. I create  this flexible service that it can be easily transfered to another application or service (from a script to an app).
The best way I could think was to get the current time and collect a response, once there was a http response then capture the time again. The difference of these times is the response it takes. Add this to an array and display this array.

Second thoughts: I have second thought that I should decouple some of the responsibility from the service and remove the duration and interval timing. This will mean all the service returns is a single ping response time.
I also thought about splitting out the timing into another service here however, I beleive this would complicate this task too much at this stage.