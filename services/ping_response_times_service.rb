require 'net/http'
require 'uri'

class PingResponseTimesService
  attr_reader :host

  def initialize(host:, time_to_ping_in_seconds: 20, ping_intervals_in_seconds: 5)
    @host = complete_url(host)
    @time_to_ping_in_seconds = time_to_ping_in_seconds
    @ping_intervals_in_seconds = ping_intervals_in_seconds
    @return_result = []
  end

  def run
    ping_end_time = (Time.now + @time_to_ping_in_seconds)
    while Time.now < ping_end_time
      response_time
      sleep(@ping_intervals_in_seconds)
    end

    @return_result
  end

  private

  def response_time
    begin
      uri = URI.parse(@host)
      ping_request_start_time = Time.now
      response = Net::HTTP.get_response(uri)
      response_end_time = Time.now - ping_request_start_time

      return false unless response
      @return_result << response_end_time

    rescue Errno::ECONNREFUSED
      return false
    end
  end

  def complete_url(host)
    return host if %w{http}.include?(host[0..3])
    "https://#{host}"
  end
end
