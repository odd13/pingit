require_relative '../services/ping_response_times_service'

RSpec.describe PingResponseTimesService do
  subject{ described_class.new(
    host: host,
    time_to_ping_in_seconds: 1,
    ping_intervals_in_seconds: 1) }

  let(:host) { 'https://gitlab.com' }

  context 'with valid results' do
    it 'returns an Array' do
      expect(subject.run).to be_a(Array)
    end

    it 'does not run outside the time requested to ping' do
      expect(subject.run.count).to eql(1)
    end
  end

  context 'when a host does not contain http' do
    let(:host) { 'gitlab.com' }
    let(:expected_host) { 'https://gitlab.com' }

    it 'adds https' do
      expect(subject.host).to eql(expected_host)
    end

    it 'returns results' do
      expect(subject.run.count).to eql(1)
    end
  end

  context 'when a host is not valid' do
    let(:host) { 'http://hostthatdoesnotexists' }

    it 'returns an error' do
      expect{subject.run}.to raise_error(an_instance_of(SocketError))
    end
  end
end
