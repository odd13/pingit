require_relative 'services/ping_response_times_service'

host = 'https://gitlab.com'

ping_response = PingResponseTimesService.new(host: host, time_to_ping_in_seconds: 300).run

puts ping_response

puts "Average ping response time to #{host}: #{(ping_response.sum / ping_response.size).round(5)} "

puts "Number of pings tested: #{ping_response.count}"
